# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

  -:in flat lambda This flat folder contains get, put, post and delete apis of the FLATAPP(table_name)     database.
  -:DUMP.js is also there for throwing messages when callback is called.
  -:Index.js is a index handler which executes the request and send to the respective api.
  -:cognitoauth is for the authorization of the apis.
  -:lib folder contains the information about using database in apis and what response will come from      the apis. 

* "version": "1.0.0"
 


### How do I get set up? ###

* Summary of set up
  
  GET.js:- get api will fetch the data from the db table according to the fields of user request if data          is not available of valid querry then it will send the blank array and if querry is not valid          then response will be simple error message.

  POST.js:- post api will store new record with all mandatory fields in the data base if fields are                valid otherwise it will send the error messages.If requested record is already there then it           will not add requested record and give error message.

  UPDATE.js:- update api update the pre-existing record in the data base after validating the entries                and return the message according to the response status_code.

  DELETE.js:- delete api will delete a record if record is existing into the database otherwise it will              send the error message.

* Dependencies
    
    "chai": "^4.2.0",
    "mocha": "^6.1.4",
    "request": "^2.88.0"

* Database configuration
   
     dynamodb = AWS.DynamoDB
	"region": "localhost",
    "endpoint": "http://192.168.5.123:8000"


    docClient =  AWS.DynamoDB.DocumentClient
	"region": "localhost",
    "endpoint": "http://192.168.5.123:8000"

    TableName: FLATAPP
    IndexName: priceSort

    
* How to run tests

     -you can run the testcases first go to the test indevidually by running commond:

     src/flat/test$ AWS_REGION=local mocha (testcase_file_name.js)

     example:  src/flat/test$ AWS_REGION=local get.spec.js

     -to test all test cases with single click simply run this commond given bellow-
      
     src/flat$ AWS_REGION=local npm test
     




