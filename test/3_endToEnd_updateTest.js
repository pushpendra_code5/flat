const request = require('request')
var expect = require('chai').expect;
console.log("******************************END_TO_END_UPDATE_TEST******************************")
describe('UPDATE flat data:', function () {
    it('update flat', function (done) {
        request.put('http://192.168.5.123:8989/pushpendra/flat/flat', {
            json: {
                "active": "true",
                "id": "wohnung1.ogb13",
                "flat_status": "sold"
              }
        }, (error, res, body) => {
            if (error) {
                console.error(error)
                return
            }

           // console.log(`statusCode: ${res.statusCode}`)
            expect(res.statusCode).to.equal(200);
            console.log(body)
            done();
        })
    });
});
