
const request = require('request')
var expect = require('chai').expect;
console.log("******************************END_TO_END_POST_TEST******************************")
describe('POST flat data:', function () {
    it('add flat', function (done) {
        request.post('http://192.168.5.123:8989/pushpendra/flat/flat', {
            json:  
                {
                    "area": 73.53,
                    "image": "arnsberg-images/OG_B-13_FloorPlan.png",
                    "flat_code": "B-13",
                    "pricePerSquare": 2900,
                    "active": "true",
                    "description": [
                        "3 Zimmer, Küche, Bad",
                        "Balkon",
                        "Gäste-WC",
                        "Waschküche + separater Keller"
                    ],
                    "flattype": "3",
                    "features": [
                        "Balkon",
                        "Gäste-WC"
                    ],
                    "floor_number_text": "im Obergeschoss",
                    "price": 213237,
                    "flat_status": "sold",
                    "name": "Haus B | Wohnung B13",
                    "no_of_rooms": 3,
                    "id": "wohnung1.ogb13",
                    "floor": 1,
                    "floor_price": 10000213237,
                    "price_sort": "true"
                },
        }, (error, res, body) => {
            if (error) {
                console.error(error)
                return
            }

            //console.log(`statusCode: ${res.statusCode}`)
            expect(res.statusCode).to.equal(200);
            console.log(body)
            
            done();
        })
    });
});
