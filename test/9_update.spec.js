var UPDATE = require('/home/force-laptop-11/src/flat/src/lambda/flat/UPDATE');
const logger = Object.assign({}, console);
var expect = require('chai').expect;
var assert = require('chai').assert;

console.log("****************************UPDATE_API******************************************");
describe('UPDATE FLAT API test-cases', function () {

    describe('UPDATE (failed) flat via id and active check ', function () {
        var updateJSON_valid_update_all_flat = {
            "httpMethod": "UPDATE",
            "body": {
                "id": "Wohnung1.OGB11",
                "active": "true"
            }
        };
        it("active should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(updateJSON_valid_update_all_flat.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("id should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.id, "string");
            done();
        });
        it("should be flat_status required return 400", function (done) {
            closure_separate_function_execution(updateJSON_valid_update_all_flat, function (err, data) {
                expect(data.statusCode).to.equal(400);
                // logger.log(JSON.stringify(data,null,6));
                done();
            });
        });
    });

    describe('UPDATE (failed) flat via active and flatStatus check ', function () {
        var updateJSON_valid_update_all_flat = {
            "httpMethod": "UPDATE",
            "body": {
                "flat_status": "sold",
                "active": "true"
            }
        };
        it("active should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(updateJSON_valid_update_all_flat.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("flat_status should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flat_status, "string");
            done();
        });
        it("should be flatId required return 400", function (done) {
            closure_separate_function_execution(updateJSON_valid_update_all_flat, function (err, data) {
                expect(data.statusCode).to.equal(400);
                // logger.log(JSON.stringify(data,null,6));
                done();
            });
        });
    });

    describe('UPDATE (failed) flat via flat Id and flatStatus check ', function () {
        var updateJSON_valid_update_all_flat = {
            "httpMethod": "UPDATE",
            "body": {
                "flat_status": "sold",
                "id": "Wohnung1.OGB11"
            }
        };
        it("flat_status should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flat_status, "string");
            done();
        });
        it("id should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.id, "string");
            done();
        });
        it("should be active required return 400", function (done) {
            closure_separate_function_execution(updateJSON_valid_update_all_flat, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });
    });


    describe('UPDATE (failed) flat via invalid flat Id and active check ', function () {
        var updateJSON_valid_update_all_flat = {
            "httpMethod": "UPDATE",
            "body": {
                "id": "e45",
                "active": "false",
                "flat_status": "sold"
            }
        };
        it("id should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.id, "string");
            done();
        });
        it("active should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(updateJSON_valid_update_all_flat.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("flat_status should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flat_status, "string");
            done();
        });
        it("active or flatId does not exit", function (done) {
            closure_separate_function_execution(updateJSON_valid_update_all_flat, function (err, data) {
                expect(data.statusCode).to.equal(400);
                // logger.log(JSON.stringify(data,null,6));
                done();

            });

        });
    });

    describe('UPDATE flat via valid flat Id and active and flat_status check ', function () {
        var updateJSON_valid_update_all_flat = {
            "httpMethod": "UPDATE",
            "body": {
                "active": "true",
                "id": "Wohnung1.OGB15",
                "flat_status": "sold",
                "floor": 2,
                "flattype": "3",
                "features": ["Balkon"],
                "pricePerSquare": 3000,
                "name": "pushp",
                "area": 80.00,
                "price": 240000.00,
                "description":["Zimmer, Küche, Bad"],
                "flat_code":"A-001",
                "floor_price":100240000,
                "floor_number_text":"abcxyzpqr",
                "no_of_rooms":3,
                "price_sort":"true",
                "image":"tuhol;kphus"

            }
        };
        it("id should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.id, "string");
            done();
        });
        it("active should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(updateJSON_valid_update_all_flat.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("flat_status should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flat_status, "string");
            done();
        });
        it("floor should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.floor, "number");
            done();
        });
        it("flattype should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flattype, "string");
            done();
        });
        it("features should be array", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flattype, "string");
            done();
        });
        it("pricePerSquare should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.pricePerSquare, "number");
            done();
        });
        it("name should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.name, "string");
            done();
        });
        it("area should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.area, "number");
            done();
        });
        it("pricePerSquare should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.pricePerSquare, "number");
            done();
        });
        it("price should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.price, "number");
            done();
        });
        it("description should be array", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.description, "array");
            done();
        });
        it("flat_code should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.flat_code, "string");
            done();
        });
        it("floor_price should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.floor_price, "number");
            done();
        });
        it("floor_number_text should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.floor_number_text, "string");
            done();
        });
        it("no_of_rooms should be number", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.no_of_rooms, "number");
            done();
        });
        it("price_sort should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.price_sort, "string");
            done();
        });
        it("image should be string", function (done) {
            assert.typeOf(updateJSON_valid_update_all_flat.body.image, "string");
            done();
        });
        it("should return updated flat data", function (done) {
            closure_separate_function_execution(updateJSON_valid_update_all_flat, function (err, data) {
                if (err) {
                    expect(err.statusCode).to.equal(400);
                    logger.log(err);
                    done(err);
                }
                else {
                    expect(data.statusCode).to.equal(200);
                    //logger.log(JSON.stringify(data,null,6));
                    done();
                }
            });
        });
        it('check valid response datatype', function (done) {
            closure_separate_function_execution(updateJSON_valid_update_all_flat, function (err, data) {
                if (err) {
                    expect(err.statusCode).to.equal(400);
                    logger.log(err);
                    done(err);
                }
                else {
                    expect(data.statusCode).to.equal(200);
                    logger.log(JSON.stringify(data,null,6));
                    //logger.log(data);
                    var item = JSON.parse(data.body);
                    assert.typeOf(item.data.active, "string");
                    assert.typeOf(item.data.id, "string");
                    assert.typeOf(item.data.floor, "number");
                    assert.typeOf(item.data.flattype, "string");
                    assert.typeOf(item.data.flat_status, "string");
                    assert.typeOf(item.data.features, "array");
                    assert.typeOf(item.data.pricePerSquare, "number");
                    assert.typeOf(item.data.name, "string");
                    assert.typeOf(item.data.area, "number");
                    assert.typeOf(item.data.price, "number");
                    assert.typeOf(item.data.description, "array");
                    assert.typeOf(item.data.flat_code, "string");
                    assert.typeOf(item.data.floor_price, "number");
                    assert.typeOf(item.data.floor_number_text, "string");
                    assert.typeOf(item.data.no_of_rooms, "number");
                    assert.typeOf(item.data.price_sort, "string");
                    assert.typeOf(item.data.image, "string");
                    done();
                }
                // done();
            });
        })
    });
});

function closure_separate_function_execution(getJSON_valid_get_all_flat, cb) {
    console['log'] = function () { return {} };
    UPDATE.execute(getJSON_valid_get_all_flat.body, {}, function (error, data) {
        if (error) {
            //return error;
            cb(error)
        }
        else {
            console['log'] = logger.log;
            cb(null, data);
        }
    });
};





