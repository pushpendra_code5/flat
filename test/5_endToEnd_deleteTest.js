const request = require('request')
var expect = require('chai').expect;
console.log("******************************END_TO_END_DELETE_TEST******************************")
describe('DELETE flat data:', function () {
    it('delete flat', function (done) {
        request.del('http://192.168.5.123:8989/pushpendra/flat/flat', {
            json: {
               "id": "wohnung1.ogb13",
                "active": "true",
            }
        }, (error, res, body) => {
            if (error) {
                console.error(error)
                return
            }

            //console.log(`statusCode: ${res.statusCode}`)
            expect(res.statusCode).to.equal(200);
            //console.log(body)
            done();
        })
    });
});