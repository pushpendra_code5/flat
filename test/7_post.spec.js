


var POST = require('/home/force-laptop-11/src/flat/src/lambda/flat/POST');
const logger = Object.assign({}, console);
var expect = require('chai').expect;
var assert = require('chai').assert;
console.log("****************************POST_API******************************************");
describe('POST FLAT API test-cases', function () {

    describe('POST (failed) flat via  active check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true"
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });
    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15"
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15"
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15"
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });


    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4"
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();
        });
        it("flat_status should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4",
                "flat_status": "available"
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();

        });
        it("flat_status should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_status, "string");
            done();

        });
        it("flat_status should be one of the allowed values:available||sold||reserved", function (done) {
            expect(postJSON.body.flat_status).to.be.oneOf(['available', 'sold', 'reserved']);
            done();
        });
        it("features should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4",
                "flat_status": "available",
                "features": [
                    "Balkon",
                    "Gäste-WC"
                ]
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();
        });    
        it("flat_status should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_status, "string");
            done();

        });
        it("flat_status should be one of the allowed values:available||sold||reserved", function (done) {
            expect(postJSON.body.flat_status).to.be.oneOf(['available', 'sold', 'reserved']);
            done();
        });
        it("features should be array.", function (done) {
            assert.typeOf(postJSON.body.features, "array");
            done();

        });
        it("area should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4",
                "flat_status": "available",
                "features": [
                    "Balkon",
                    "Gäste-WC"
                ],
                "area": 91.48
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();

        });
        it("flat_status should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_status, "string");
            done();

        });
        it("flat_status should be one of the allowed values:available||sold||reserved", function (done) {
            expect(postJSON.body.flat_status).to.be.oneOf(['available', 'sold', 'reserved']);
            done();
        });
        it("features should be array.", function (done) {
            assert.typeOf(postJSON.body.features, "array");
            done();

        });
        it("area should be number.", function (done) {
            assert.typeOf(postJSON.body.area, "number");
            done();

        });
        it("pricePerSquare should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4",
                "flat_status": "available",
                "features": [
                    "Balkon",
                    "Gäste-WC"
                ],
                "area": 91.48,
                "pricePerSquare": 2900
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();

        });
        it("flat_status should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_status, "string");
            done();

        });
        it("flat_status should be one of the allowed values:available||sold||reserved", function (done) {
            expect(postJSON.body.flat_status).to.be.oneOf(['available', 'sold', 'reserved']);
            done();
        });
        it("features should be array.", function (done) {
            assert.typeOf(postJSON.body.features, "array");
            done();

        });
        it("area should be number.", function (done) {
            assert.typeOf(postJSON.body.area, "number");
            done();

        });
        it("pricePerSquare should be number.", function (done) {
            assert.typeOf(postJSON.body.pricePerSquare, "number");
            done();

        });
        it("price should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });
    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4",
                "flat_status": "available",
                "features": [
                    "Balkon",
                    "Gäste-WC"
                ],
                "area": 91.48,
                "pricePerSquare": 2900,
                "price": 265292
            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();

        });
        it("flat_status should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_status, "string");
            done();

        });
        it("flat_status should be one of the allowed values:available||sold||reserved", function (done) {
            expect(postJSON.body.flat_status).to.be.oneOf(['available', 'sold', 'reserved']);
            done();
        });
        it("features should be array.", function (done) {
            assert.typeOf(postJSON.body.features, "array");
            done();

        });
        it("area should be number.", function (done) {
            assert.typeOf(postJSON.body.area, "number");
            done();

        });
        it("pricePerSquare should be number.", function (done) {
            assert.typeOf(postJSON.body.pricePerSquare, "number");
            done();

        });
        it("price should be number.", function (done) {
            assert.typeOf(postJSON.body.price, "number");
            done();

        });
        it("floor_price should be required.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data,null,6));
                done();
            });
        });

    });

    describe('POST (failed) flat  check ', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {

                "active": "true",
                "name": "Haus B | Wohnung B15",
                "id": "wohnung1.ogb15",
                "floor": 1,
                "flattype": "4",
                "flat_status": "available",
                "features": [
                    "Balkon",
                    "Gäste-WC"
                ],
                "area": 91.48,
                "pricePerSquare": 2900,
                "price": 265292,
                "floor_price": 10000265292,
                "description": [
                    "4 Zimmer, Küche, Bad",
                    "Großzügige Balkon",
                    "Gäste-WC",
                    "Waschküche + separater Keller"
                ],
                "flat_code": "B-15",
                "floor_number_text": "im Obergeschoss",
                "image": "arnsberg-images/OG_B-15_FloorPlan.png",
                "no_of_rooms": 4,
                "price_sort": "true"

            }
        };
        it("active should be string.", function (done) {
            assert.typeOf(postJSON.body.active, "string");
            done();

        });

        it("active should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("name should be string.", function (done) {
            assert.typeOf(postJSON.body.name, "string");
            done();

        });
        it("id should be string.", function (done) {
            assert.typeOf(postJSON.body.id, "string");
            done();

        });
        it("floor should be number.", function (done) {
            assert.typeOf(postJSON.body.floor, "number");
            done();

        });
        it("flattype should be string.", function (done) {
            assert.typeOf(postJSON.body.flattype, "string");
            done();

        });
        it("flat_status should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_status, "string");
            done();

        });
        it("flat_status should be one of the allowed values:available||sold||reserved", function (done) {
            expect(postJSON.body.flat_status).to.be.oneOf(['available', 'sold', 'reserved']);
            done();
        });
        it("features should be array.", function (done) {
            assert.typeOf(postJSON.body.features, "array");
            done();

        });
        it("area should be number.", function (done) {
            assert.typeOf(postJSON.body.area, "number");
            done();

        });
        it("pricePerSquare should be number.", function (done) {
            assert.typeOf(postJSON.body.pricePerSquare, "number");
            done();

        });
        it("price should be number.", function (done) {
            assert.typeOf(postJSON.body.price, "number");
            done();

        });
        it("floor_price should be number.", function (done) {
            assert.typeOf(postJSON.body.floor_price, "number");
            done();

        });
        it("description should be array.", function (done) {
            assert.typeOf(postJSON.body.description, "array");
            done();

        });
        it("flat_code should be string.", function (done) {
            assert.typeOf(postJSON.body.flat_code, "string");
            done();

        });
        it("floor_number_text should be string.", function (done) {
            assert.typeOf(postJSON.body.floor_number_text, "string");
            done();

        });
        it("image should be string.", function (done) {
            assert.typeOf(postJSON.body.image, "string");
            done();

        });
        it("no_of_rooms should be number.", function (done) {
            assert.typeOf(postJSON.body.no_of_rooms, "number");
            done();

        });
        it("price_sort should be string.", function (done) {
            assert.typeOf(postJSON.body.price_sort, "string");
            done();

        });
        it("price_sort should be one of the allowed values:true||false", function (done) {
            expect(postJSON.body.price_sort).to.be.oneOf(['true', 'false']);
            done();
        });


        it("Post new flat :", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    done();
                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }
            });
        });

    });

    describe('POST(failed) flat via duplicate flat_id check', function () {
        var postJSON = {
            "httpMethod": "POST",
            "body": {
                "area": 91.48,
                "image": "arnsberg-images/OG_B-15_FloorPlan.png",
                "flat_code": "B-15",
                "pricePerSquare": 2900,
                "active": "true",
                "description": [
                    "4 Zimmer, Küche, Bad",
                    "Großzügige Balkon",
                    "Gäste-WC",
                    "Waschküche + separater Keller"
                ],
                "flattype": "4",
                "features": [
                    "Balkon",
                    "Gäste-WC"
                ],
                "floor_number_text": "im Obergeschoss",
                "price": 265292,
                "flat_status": "available",
                "name": "Haus B | Wohnung B15",
                "no_of_rooms": 4,
                "id": "wohnung1.ogb15",
                "floor": 1,
                "floor_price": 10000265292,
                "price_sort": "true"
            }
        };
        it("id already exit.", function (done) {
            closure_separate_function_execution(postJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                // logger.log(JSON.stringify(data,null,6));
                done();
            });
        });
    });
});


function closure_separate_function_execution(postJSON, cb) {
    console['log'] = function () { return {} };
    POST.execute(postJSON.body, function (error, data) {
        if (error) {
            cb(error)
        }
        else {
            console['log'] = logger.log;
            cb(null, data);
        }
    });
};