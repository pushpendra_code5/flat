var DELETE = require('/home/force-laptop-11/src/flat/src/lambda/flat/DELETE');
const logger = Object.assign({}, console);
var expect = require('chai').expect;
var assert = require('chai').assert;

console.log("****************************DELETE_API******************************************");
describe('DELETE FLAT API test-cases', function () {


    describe('DELETE (failed) flat via active check ', function () {
        var deleteJSON = {
            "httpMethod": "DELETE",
            "body": {
                "active": "true",
            }
        };
        it("active should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(deleteJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("id should be required.", function (done) {
            closure_separate_function_execution(deleteJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                done();
            });

        });
    });

    describe('delete (failed) flat via invalid id and active check ', function () {
        var deleteJSON = {
            "httpMethod": "DELETE",
            "body": {
                "active": "false",
                "id": "e45",
            }
        };
        it("active should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(deleteJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("id should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });
        it("active or id does not exit", function (done) {
            closure_separate_function_execution(deleteJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                done();

            });

        });
    });
    describe('delete (failed) flat via active, id and additional property ', function () {
        var deleteJSON = {
            "httpMethod": "DELETE",
            "body": {
                "active": "true",
                "id": "a410",
                "name": "a",

            }
        };
        it("active should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(deleteJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("id should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });
        it("should not have additional properties...only active and id required.", function (done) {
            closure_separate_function_execution(deleteJSON, function (err, data) {
                expect(data.statusCode).to.equal(400);
                done();

            });

        });
    });

    describe('delete flat via valid id and active property ', function () {
        var deleteJSON = {
            "httpMethod": "DELETE",
            "body": {
                "active": "true",
                "id": "Wohnung1.OGB15"
            }
        };

        it("active should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });
        it("active should be one of the allowed values:true||false", function (done) {
            expect(deleteJSON.body.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("id should be string", function (done) {
            assert.typeOf(deleteJSON.body.active, "string");
            done();
        });

        it("flat is deleted successfully.", function (done) {
            closure_separate_function_execution(deleteJSON, function (err, data) {
                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    done();
                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }
            });
        });
    });
});

function closure_separate_function_execution(deleteJSON, cb) {
    console['log'] = function () { return {} };
    DELETE.execute(deleteJSON.body, function (error, data) {
        if (error) {
            //return error;
            cb(error)
        }
        else {
            console['log'] = logger.log;
            //logger.log(data);
            cb(null, data);
        }
    });
};