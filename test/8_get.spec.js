var GET = require('/home/force-laptop-11/src/flat/src/lambda/flat/GET');

const logger = Object.assign({}, console);
var expect = require('chai').expect;
var assert = require('chai').assert;

console.log("****************************GET_API******************************************");
describe('GET FLAT API test-cases', function () {

    describe('GET flat via active check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "active": "true"
            }
        };
        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.active, "string");
        });

        it("should be allowed values:true||false", function (done) {
            expect(getJSON_valid_get_all_flat.queryStringParameters.active).to.be.oneOf(['true', 'false']);
            done();
        });
        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })
                } else {
                    expect(data.statusCode).to.equal(400);
                    // logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                    // done();
                }

            });
        });

    });

    describe('GET flat via features check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "features": "Balkon"
            }
        };

        it('feature_name should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.features, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                //logger.log(JSON.stringify(data, null, 6));
                expect(data.statusCode).to.equal(200);
                done();
            });
        });


        it('check valid response datatype', function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                if (err) {
                    expect(err.statusCode).to.equal(400);
                    logger.log(err);
                    done(err);
                }
                else {
                    //logger.log(JSON.stringify(data, null, 6));
                    expect(data.statusCode).to.equal(200);
                    get_valid_response_check(data, function () {
                        done();
                    })
                }
            });
        })
    });


    describe('GET flat via floor check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "floor": "1"
            }
        };
        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })

                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });


    });
    describe('GET flat via multiple floorNo check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "floor": "1,3"
            }
        };
        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })

                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });


    });

    describe('GET flat via floor and active check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "floor": "1",
                "active": "true"
            }
        };
        it('active should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.active, "string");
        });

        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })

                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });


    });

    describe('GET flat via floor and active and flat type check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "floor": "1",
                "active": "true",
                "flattype": "4"
            }
        };
        it('flattype should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flattype, "string");
        });

        it('active should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.active, "string");
        });

        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })

                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });


    });

    describe('GET flat via floor and active and flat type and flat_status check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "floor": "1",
                "active": "true",
                "flattype": "4",
                "flat_status": "available"
            }
        };
        it('flat_status should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flat_status, "string");
        });

        it('flattype should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flattype, "string");
        });

        it('active should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.active, "string");
        });

        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })

                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });


    });

    describe('GET flat via floor and active and flat type and flat-status and features check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "floor": "1",
                "active": "true",
                "flattype": "4",
                "flat_status": "available",
                "features":"Balkon"
            }
        };
        it('features should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.features, "string");
        });
        it('flat_status should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flat_status, "string");
        });

        it('flattype should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flattype, "string");
        });

        it('active should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.active, "string");
        });

        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })

                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });


    });


    describe('GET flat via flat_type check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "flattype": "4"
            }
        };
        it('flatType should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flattype, "string");
        });

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })
                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        });

    });


    describe('GET flat(failed) via invalid flat_type check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "flattype": 5
            }
        };

        it("flat_type should be string.", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                // logger.log(JSON.stringify(data, null, 6));
                expect(data.statusCode).to.equal(400);
                done();
            });
        });
    });

    describe('GET flat via flatStatus check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "flat_status": "sold"
            }
        };

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    // logger.log(JSON.stringify(data, null, 6));
                    get_valid_response_check(data, function () {
                        done();
                    })
                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }
            });

        })
        it('should be string type', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flat_status, "string");
        });
        it("flatStatus should be one of allowed values: available || reserved || sold ", function (done) {
            expect(getJSON_valid_get_all_flat.queryStringParameters.flat_status).to.be.oneOf(['available', 'reserved', 'sold']);
            done();
        });
    });
    describe('GET flat via pagination check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "lastEvaluatedKeyactive": "",
                "lastEvaluatedKeyid": "",
                "lastEvaluatedKeyprice": ""
            }
        };

        it("should return flat data", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                //  logger.log(JSON.stringify(data, null, 6));
                if (data.statusCode == 200) {
                    expect(data.statusCode).to.equal(200);
                    get_valid_response_check(data, function () {
                        done();
                    })
                } else {
                    expect(data.statusCode).to.equal(400);
                    //logger.log(JSON.stringify(data, null, 6));
                    done(new Error(data.body));
                }

            });

        })
    });
    describe('GET flat via priceSort check ', function () {
        var getJSON_valid_get_all_flat = {
            "httpMethod": "GET",
            "queryStringParameters": {
                "priceSort": "true"
            }
        };

        it("should return flat data in sorted order by price", function (done) {
            closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {
                //  logger.log(JSON.stringify(data, null, 6));
                get_valid_response_check(data, function () {
                    done();
                })
            });

        })
        it('should be string type : true || false', function () {
            assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.priceSort, "string");
        });
    });
});

describe('GET flat via floor and active and flat type and flat-status and features with priceSort check ', function () {
    var getJSON_valid_get_all_flat = {
        "httpMethod": "GET",
        "queryStringParameters": {
            "floor": "1",
            "active": "true",
            "flattype": "4",
            "flat_status": "available",
            "features":"Balkon",
            "priceSort":"true"
        }
    };

    it('priceSort should be string type', function () {
        assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.features, "string");
    });
    
    it('features should be string type', function () {
        assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.features, "string");
    });
    it('flat_status should be string type', function () {
        assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flat_status, "string");
    });

    it('flattype should be string type', function () {
        assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.flattype, "string");
    });

    it('active should be string type', function () {
        assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.active, "string");
    });

    it('should be string type', function () {
        assert.typeOf(getJSON_valid_get_all_flat.queryStringParameters.floor, "string");
    });

    it("should return flat data", function (done) {
        closure_separate_function_execution(getJSON_valid_get_all_flat, function (err, data) {

            if (data.statusCode == 200) {
                expect(data.statusCode).to.equal(200);
                // logger.log(JSON.stringify(data, null, 6));
                get_valid_response_check(data, function () {
                    done();
                })

            } else {
                expect(data.statusCode).to.equal(400);
                //logger.log(JSON.stringify(data, null, 6));
                done(new Error(data.body));
            }

        });

    });


});


function closure_separate_function_execution(getJSON_valid_get_all_flat, cb) {
    console['log'] = function () { return {} };
    GET.execute(getJSON_valid_get_all_flat.queryStringParameters, function (error, data) {
        if (error) {
            cb(error);
        }
        else {
            console['log'] = logger.log;
            cb(null, data);
        }
    })
}

function get_valid_response_check(data, call) {
    var item = JSON.parse(data.body);
    // console.log(item.data)
    var len = item.data.length;
    // logger.log(len);
    for (i = 0; i < len; i++) {
        assert.typeOf(item.data[i].active, "string");
        assert.typeOf(item.data[i].id, "string");
        assert.typeOf(item.data[i].floor, "number");
        assert.typeOf(item.data[i].flattype, "string");
        assert.typeOf(item.data[i].flat_status, "string");
        assert.typeOf(item.data[i].features, "array");
        assert.typeOf(item.data[i].pricePerSquare, "number");
        assert.typeOf(item.data[i].name, "string");
        assert.typeOf(item.data[i].area, "number");
        assert.typeOf(item.data[i].price, "number");
        assert.typeOf(item.data[i].description, "array");
        assert.typeOf(item.data[i].flat_code, "string");
        assert.typeOf(item.data[i].floor_price, "number");
        assert.typeOf(item.data[i].floor_number_text, "string");
        assert.typeOf(item.data[i].no_of_rooms, "number");
        assert.typeOf(item.data[i].price_sort, "string");
        assert.typeOf(item.data[i].image, "string");
    }
    call();
}