
const keyBase64 = "MzRua3NmMGoyM2tuZnNwam1hZGFzZGFzZGZhc2Rhc2Q=";
const ivBase64 = 'amtic2RnYmprc252c3NvZQ==';

var crypto = require('crypto');

function getAlgorithm(keyBase64) {

    var key = Buffer.from(keyBase64, 'base64');
    switch (key.length) {
        case 16:
            return 'aes-128-cbc';
        case 32:
            return 'aes-256-cbc';

    }

    throw new Error('Invalid key length: ' + key.length);
}

function decrypt1(messagebase64, keyBase64, ivBase64) {
    var key = Buffer.from(keyBase64, 'base64');
    var iv = Buffer.from(ivBase64, 'base64');
    // var key = "MzRua3NmMGoyM2tuZnNwam1hZGFzZGFzZGZhc2Rhc2Q=";
    // var iv = "amtic2RnYmprc252c3NvZQ==";
    try{
        var decipher = crypto.createDecipheriv(getAlgorithm(keyBase64), key, iv);
        decipher.update(messagebase64, 'base64');
        return decipher.final().toString();
    }catch(e){
        console.log(e);
        return "Null";
    }
}

module.exports ={
    "decrypt": function decrypt(cipherText){
        return decrypt1(cipherText, keyBase64, ivBase64)
    }
};