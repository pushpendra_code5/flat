///// ...................................... start default setup ............................................////
let mode,sns,dynamodb,docClient,S3;
const AWS 			= require('aws-sdk');
const response 		= require('./lib/response.js');
const cryto = require('./lib/crypto');
AWS.config.region 	= "eu-central-1";
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider()

if(process.env.AWS_REGION == "local"){
	mode 		= "offline";
	sns 		= require('../../../offline/sns');
	docClient 	= require('../../../offline/dynamodb').docClient;
	S3 			= require('../../../offline/S3');
	// dynamodb = require('../../../offline/dynamodb').dynamodb;
}else{
	mode 		= "online";
	sns 		= new AWS.SNS();
	docClient 	= new AWS.DynamoDB.DocumentClient({});
	S3 			= new AWS.S3();
	// dynamodb = new AWS.DynamoDB();
}
///// ...................................... end default setup ............................................////

//modules defined here
// const uuid 		= require('uuid');
const Ajv 			= require('ajv');
const setupAsync 	= require('ajv-async');
const ajv 			= setupAsync(new Ajv);

const PutSchema = {
  "$async":true,
  "type":"object",
  "required":["accesstoken","oldpassword","newpassword"],
  "properties":{
    "accesstoken":{
		"type":"string"
	},
	"oldpassword":{
		"type":"string"
	},
	"newpassword":{
		"type":"string"
	}
  }
};

const validate = ajv.compile(PutSchema);

//call another lambda
// const execute_lambda = require('./lib/lambda')('sample2');

module.exports={execute};

/**
 * This is the Promise caller which will call each and every function based
 * @param  {[type]}   data     [content to manipulate the data]
 * @param  {Function} callback [need to send response with]
 * @return {[type]}            [description]
 */
function execute(data,callback){
	console.log(data);
	if(typeof data == "string"){
		try{
			data = JSON.parse(data);
		}catch(excep){
			delete data;
		}
	}
	validate_all(validate,data)
		.then(function(result){
			return change_password(result);
		})
		.then(function(result){
				response({code:200,body:result},callback);
		})
		.catch(function(err){
			response({code:400,err:{err}},callback);
		})
}

/**
 * validate the data to the categories
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function validate_all (validate,data) {
	return new Promise((resolve,reject)=>{
		validate(data).then(function (res) {
			console.log(res);
		    resolve(res);
		}).catch(function(err){
		  console.log(JSON.stringify( err,null,6) );
		  reject(err.errors[0].dataPath+" "+err.errors[0].message);
		})
	})
}

function change_password(result){
	return new Promise((resolve,reject)=>{
		var params = {
			AccessToken: result.accesstoken, /* required */
			PreviousPassword: result.oldpassword, /* required */
			ProposedPassword: result.newpassword /* required */
			};
			if(data.oldpassword){
				data.oldpassword = cryto.decrypt(data.oldpassword);
			}
			if(data.newpassword){
				data.newpassword = cryto.decrypt(data.newpassword);
			}
		  cognitoidentityserviceprovider.changePassword(params, function(err, data) {
			if (err){
				reject(err);
			}
			else{
				console.log(data);
				resolve(data);
			}
		  });
	});
}