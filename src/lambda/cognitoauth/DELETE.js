///// ...................................... start default setup ............................................////
let mode,sns,dynamodb,docClient,S3;
const AWS 		= require('aws-sdk');
const response 	= require('./lib/response.js');
const cryto = require('./lib/crypto');
AWS.config.region 	= "eu-central-1";
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider()

if(process.env.AWS_REGION == "local"){
	mode 		= "offline";
	sns 		= require('../../../offline/sns');
	docClient 	= require('../../../offline/dynamodb').docClient;
	// S3 			= require('../../../offline/S3');
	// dynamodb = require('../../../offline/dynamodb').dynamodb;
}else{
	mode 		= "online";
	sns 		= new AWS.SNS();
	docClient 	= new AWS.DynamoDB.DocumentClient({});
	// S3 			= new AWS.S3();
	// dynamodb = new AWS.DynamoDB();
}
///// ...................................... end default setup ............................................////

//modules defined here
const uuid 		= require('uuid');
//call another lambda
// const execute_lambda = require('./lib/lambda')('sample2');
const Ajv 			= require('ajv');
const setupAsync 	= require('ajv-async');
const ajv 			= setupAsync(new Ajv);

const getSchema = {
  "$async":true,
  "type":"object",
  "additionalProperties": false,
  "properties":{
  	"mode":{"type":"string",
  		"enum":["forgot","confirm","resend"]
  	},
    "clientId":{"type":"string"},
    "username":{"type":"string"},
    "confirmationCode":{"type":"string"},
    "password":{"type":"string"},
	"UserContextData":{"type":"object"},
	"userpool":{"type":"string"}
  }
};

//   Username: 'STRING_VALUE', /* required */
// ClientId: 'STRING_VALUE', /* required */

// ClientId: 'STRING_VALUE', /* required */
//   ConfirmationCode: 'STRING_VALUE',  required 
//   Password: 'STRING_VALUE', /* required */
//   Username: 'STRING_VALUE', /* required */

const validate = ajv.compile(getSchema);

/**
 * This is the Promise caller which will call each and every function based
 * @param  {[type]}   data     [content to manipulate the data]
 * @param  {Function} callback [need to send response with]
 * @return {[type]}            [description]
 */

function execute(data,callback){
	if(typeof data == "string"){
		data = JSON.parse(data);
	}
	if(data.password){
		data.password = cryto.decrypt(data.password);
	}
	validate_all(validate,data)
		.then(function(result){
			console.log("this is the result");
			console.log(result);
			if(result.mode =="forgot"){
				return forgot(result);
			}else if(result.mode == "confirm"){
				return confirm_forgot(result);
			}else if(result.mode == "resend"){
				return resendConfirmationCode(result);
			}else{
				return Promise.reject("Please select valid input");
			}
		})
		.then(function(result){
				response({code:200,body:result},callback);
		})
		.catch(function(err){
			response({code:400,err:{err}},callback);
		})
}

/**
 * forgot description
 * @param  {[type]} result [description]
 * @return {[type]}        [description]
 */
function forgot(data){
	return new Promise((resolve,reject)=>{
		var params = {
			UserPoolId: data.userpool,
			Filter: 'email = \"'+data.username+'\"',
			Limit: 1,
		  };
		  cognitoidentityserviceprovider.listUsers(params, function(err, data2) {
			if (err){
				reject(err);
			}
			else{
				console.log(data2);
				if(typeof data2 == "string"){
					data2 = JSON.parse(data2);
				}
				if(data2 && data2.Users && data2.Users[0] ){
					var newparams={
						ClientId: data.clientId,
						Username: data2.Users[0].Username
					};
					if(data.UserContextData != undefined){
						params.UserContextData = data.UserContextData;
					}
					cognitoidentityserviceprovider.forgotPassword(newparams, function(err, data) {
						if (err) reject(err.message) // an error occurred
						else resolve(data);           // successful response
					});
				}else{
					reject("Not a valid Username");
				}

			}
		  });
	})
}

/**
 * confirm_forgot description
 * @param  {[type]} result [description]
 * @return {[type]}        [description]
 */
function confirm_forgot(data){
	return new Promise((resolve,reject)=>{
		var params = {
			UserPoolId: data.userpool,
			Filter: 'email = \"'+data.username+'\"',
			Limit: 1,
		  };
		  cognitoidentityserviceprovider.listUsers(params, function(err, data2) {

			var params={
				ClientId: data.clientId,
				ConfirmationCode: data.confirmationCode,
				Password: data.password,
				Username: data2.Users[0].Username
			};
			cognitoidentityserviceprovider.confirmForgotPassword(params, function(err, data) {
			if (err) reject(err.message) // an error occurred
			else resolve(data);           // successful response
			});
		});
	})
}

/**
 * validate the data to the categories
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function validate_all (validate,data) {
	return new Promise((resolve,reject)=>{
		validate(data).then(function (res) {
			console.log(res);
		    resolve(res);
		}).catch(function(err){
		  console.log(JSON.stringify( err,null,6) );
		  reject(err.errors[0].dataPath+" "+err.errors[0].message);
		})
	})
}

/**
 * resend confirmation code
 */
function resendConfirmationCode(result){
	return new Promise((resolve,reject)=>{
	var params = {
			UserPoolId: result.userpool,
			Filter: 'email = \"'+result.username+'\"',
			Limit: 1
		};
		cognitoidentityserviceprovider.listUsers(params, function(err, data2) {
			var params = {
					ClientId: result.clientId, /* required */
					Username: data2.Users[0].Username /* required */
				};
				console.log(result);
				cognitoidentityserviceprovider.resendConfirmationCode(params, function(err, data) {
					if (err){
						reject(err.message)
					}else{
						resolve(data);
					}
					});
		});
	})
}

/**
 * last line of code
 * @type {Object}
 */
module.exports={execute};