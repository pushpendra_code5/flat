///// ...................................... start default setup ............................................////
let mode, sns, dynamodb, docClient, S3;
const AWS = require('aws-sdk');

if (process.env.AWS_REGION == "local") {
	mode = "offline";
	docClient = require('../../../offline/dynamodb').docClient;
} else {
	mode = "online";
	docClient = new AWS.DynamoDB.DocumentClient({});
}
///// ...................................... end default setup ............................................////

const Ajv = require('ajv');
const setupAsync = require('ajv-async');
const ajv = setupAsync(new Ajv);

const getSchema = {
	"$async": true,
	"type": "object",
	"properties": {
		"username": { "type": "string" }
	}
};

const validate = ajv.compile(getSchema);

module.exports = { execute };

/**
 * This is the Promise caller which will call each and every function based
 * @param  {[type]}   data     [content to manipulate the data]
 * @param  {Function} callback [need to send response with]
 * @return {[type]}            [description]
 */


function execute(data, callback) {
	response({ code: 400, err: { err: "Method not implemented" } }, callback);
}

/**
 * validate the data to the categories
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function validate_all(validate, data) {
	return new Promise((resolve, reject) => {
		validate(data).then(function (res) {
			console.log(res);
			resolve(res);
		}).catch(function (err) {
			console.log(JSON.stringify(err, null, 6));
			reject(err.errors[0].dataPath + " " + err.errors[0].message);
		})
	})
}
