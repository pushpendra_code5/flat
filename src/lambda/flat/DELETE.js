//***********************************************************FLAT_API******************************************************** */

///// ...................................... start default setup ............................................////
let mode, docClient, S3;
const AWS = require('aws-sdk');
const response = require('./lib/response.js');
var database = require('./lib/database.js');


if (process.env.AWS_REGION == "local") {
	mode = "offline";
	docClient = require('../../../offline/dynamodb').docClient;
	// S3 = require('../../../offline/S3');
} else {
	mode = "online";
	docClient = new AWS.DynamoDB.DocumentClient({});
	// S3 = new AWS.S3();
}
///// ...................................... end default setup ............................................////

//modules defined here

const Ajv = require('ajv');
const setupAsync = require('ajv-async');
const ajv = setupAsync(new Ajv);

//delete schema of the flat
const DeleteSchema = {
	"$async": true,
	"type": "object",
	"additionalProperties": false,
	required:	["id"],
	
	"properties":
	{
	"id": {
		"type": "string"
	}
	
}
};

const validate = ajv.compile(DeleteSchema);


module.exports = { execute };

/**
 * This is the Promise caller which will call each and every function based
 * @param  {[type]}   data     [content to manipulate the data]
 * @param  {Function} callback [need to send response with]
 * @return {[type]}            [description]
 */
function execute(data, callback) {
	console.log(data);
	if (typeof data == "string") {
		try {
			data = JSON.parse(data);
		} catch (excep) {
			delete data;
		}
	}
	validate_all(validate, data)
		.then(function (result) {
			console.log(result, 'delete item')
			return deleteItem(result);
		})
		.then(function (result) {
			response({ code: 200, body: result }, callback);
		})
		.catch(function (err) {
			response({ code: 400, err: { err } }, callback);
		})
}

/**
 * validate the data to the categories
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function validate_all(validate, data) {
	return new Promise((resolve, reject) => {
		validate(data).then(function (res) {
			console.log(res);
			resolve(res);
		}).catch(function (err) {
			console.log(JSON.stringify(err, null, 6));
			reject(err.errors[0].dataPath + " " + err.errors[0].message);
		})
	})
}

/**
 * login
 * @param  {[type]} data data=> username,password,clientId,userpool,contextdata
 * @return {[type]}      resolve
 */
function deleteItem(data) {
	return new Promise((resolve, reject) => {
		var params = {
			TableName:database.Table[0].TableName,
			Key: {
					active: "false",
					id: (data.id).toLowerCase()
			},
			ConditionExpression: 'attribute_exists(id)',
		    //  ConditionExpression: "name = :val",
			//  ExpressionAttributeValues: {
			//  	   ":val": (data.name)
	//    },
		
	};
		console.log(params);
		docClient.delete(params, function (err, data) {
			if (err) reject(err.message+" "+"active or id does not exit") // an error occurred
			else
				data['success'] = "flat deleted successfully."
			resolve(data);           // successful response
		});

	});
}