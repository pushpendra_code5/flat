

//****************************************************FLAT_API***********************************************************

///// ...................................... start default setup ............................................////
let mode, docClient, S3;
const AWS = require('aws-sdk');
const response = require('./lib/response.js');
var database = require('./lib/database.js');


if (process.env.AWS_REGION == "local") {
	mode = "offline";
	docClient = require('../../../offline/dynamodb').docClient;
	// S3 = require('../../../offline/S3');
} else {
	mode = "online";
	docClient = new AWS.DynamoDB.DocumentClient({});
	// S3 = new AWS.S3();
}
///// ...................................... end default setup ............................................////

//modules defined here

const Ajv = require('ajv');
const setupAsync = require('ajv-async');
const ajv = setupAsync(new Ajv);
const createdDate = new Date().getTime();
console.log(new Date(createdDate));
//post schema
const PostSchema = {
	"$async": true,
	"type": "object",
	"additionalProperties": false,
	"required": ['name',"id","floor","flattype","flat_status",
	                        "features","area","pricePerSquare","price","floor_price"],
	"properties":
	{
	"area": {
		"type": "number",
	},
	"description": {
		"type": "array",
	},
	"features": {
		"type": "array",
	},
	"flat_code": {
		"type": "string",
	},
	"flat_status": {
		"type": "string",
		"enum": [
			"available",
			"reserved",
			"sold",
			"AVAILABLE",
			"RESERVED",
			"SOLD",
			"Available",
			"Reserved",
			"Sold"
		]
	},
	"flattype": {
		"type": "string",
	},
	"floor": {
		"type": "number",
	},
	"floor_number_text": {
		"type": "string",
	},
	"floor_price": {
		"type": "number",
	},
	"id": {
		"type": "string",
	},
	"image": {
		"type": "string",
	},
	"name": {
		"type": "string",
	},
	"no_of_rooms": {
		"type": "number",
		"maximum":5,
		"minimum":1
	},
	"price": {
		"type": "number",
	},
	"price_sort": {
		"type": "string",
	},
	"pricePerSquare": {
		"type": "number",
	},
}
};

const validate = ajv.compile(PostSchema);


module.exports = { execute };

/**
 * This is the Promise caller which will call each and every function based
 * @param  {[type]}   data     [content to manipulate the data]
 * @param  {Function} callback [need to send response with]
 * @return {[type]}            [description]
 */
function execute(data, callback) {
	console.log(data);
	if (typeof data == "string") {
		try {
			data = JSON.parse(data);
		} catch (excep) {
			delete data;
		}
	}
	validate_all(validate, data)
		.then(function (result) {
			console.log(result, 'create configrtor')
			return crateConfigrator(result);
		})
		.then(function (result) {
			response({ code: 200, body: result }, callback);
		})
		.catch(function (err) {
			response({ code: 400, err: { err } }, callback);
		})
}

/**
 * validate the data to the categories
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function validate_all(validate, data) {
	return new Promise((resolve, reject) => {
		
		validate(data).then(function (res) {
			console.log(res);
			resolve(res);
		}).catch(function (err) {
			console.log(JSON.stringify(err, null, 6));
			reject(err.errors[0].dataPath + " " + err.errors[0].message);
		})
	})
}

/**
 * login
 * @param  {[type]} data data=> username,password,clientId,userpool,contextdata
 * @return {[type]}      resolve
 */
function crateConfigrator(data) {
	return new Promise((resolve, reject) => {
	//add flat
		var params = {
			TableName: database.Table[0].TableName,
			Item: {
					"active":"true",
					"area": data.area,
					"description":data.description,
					"features" :data.features,
                    "flat_code":data.flat_code,
					"floor":data.floor,
					"flat_status" :(data.flat_status).toLowerCase(),
					"flattype": (data.flattype),
					"floor_number_text":data.floor_number_text,
					"floor_price":data.floor_price,
					"id":(data.id).toLowerCase(),
					"image":data.image,
					"name":data.name,
					"no_of_rooms":data.no_of_rooms,
					"price":data.price,
					"price_sort":data.price_sort,
					"pricePerSquare" :data.pricePerSquare,
					"createdAt" :createdDate.toString(),
			},
			ConditionExpression: 'attribute_not_exists(id)'
		};
		console.log("Post data", params)
		docClient.put(params, function (err, data) {
			if (err) reject(err.message+""+"flatId already exits or validation failed.") // an error occurred
			else
				data['success'] = "flat created successfully"
			resolve(data);           // successful response
		});

	});
}