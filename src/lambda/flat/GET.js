//******************************************************************************************************************************************* /

///// ...................................... start default setup ............................................////
let mode, docClient, S3;
const AWS = require('aws-sdk');
const response = require('./lib/response.js');
const database = require('./lib/database.js');
// console.log(process.env.AWS_REGION);
if (process.env.AWS_REGION == "local") {
	mode = "offline";
	docClient = require('../../../offline/dynamodb').docClient;
} else {
	mode = "online";
	docClient = new AWS.DynamoDB.DocumentClient({});
}
///// ...................................... end default setup ............................................////

// modules defined here
const Ajv = require('ajv');
const setupAsync = require('ajv-async');
const ajv = setupAsync(new Ajv);

//schema for the get Flats
const getSchema = {
	"$async": true,
	"type": "object",
	"additionalProperties": false,

	"properties":
	{
		"active": {
			"type": "string",
			"enum": [
				"true",
				"false",
				"TRUE",
				"FALSE",
				"True",
				"False"
			]
		},
		"floor": {
			"type": "string",

		},
		"flattype": {
			"type": "string",
		},
		"flat_status": {
			"type": "string",
			"enum": [
				"available",
				"reserved",
				"sold",
				"Available",
				"Sold",
				"Reserved",
				"AVAILABLE",
				"RESERVED",
				"SOLD"
			]
		},
		"priceSort": {
			"type": "string"
		},
		"features": {
			"type": "string",
		},
		"lastEvaluatedKeyid": {
			"type": "string"
		},
		"lastEvaluatedKeyprice": {
			"type": "string"
		},
		"lastEvaluatedKeyactive": {
			"type": "string"
		},
		"limit":{
			type:"string"
		}
	}
};
const validate = ajv.compile(getSchema);

module.exports = { execute };

/**
 * This is the Promise caller which will call each and every function based
 * @param  {[type]}   data     [content to manipulate the data]
 * @param  {Function} callback [need to send response with]
 * @return {[type]}            [description]
 */


function execute(data, callback) {
	validate_all(validate, data)
		.then(function (result) {
			return getFlats(result);
		})
		.then(function (result) {
			response({ code: 200, body: result }, callback);
		})
		.catch(function (err) {
			response({ code: 400, err: { err } }, callback);
		})
}

/**
 * validate the data to the categories
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function validate_all(validate, data) {
	return new Promise((resolve, reject) => {
		validate(data).then(function (res) {
			console.log(res);
			resolve(res);
		}).catch(function (err) {
			console.log(JSON.stringify(err, null, 6));
			reject(err.errors[0].dataPath + " " + err.errors[0].message);
		})
	})
}


function getFlats(data) {
	console.log(database);
	var params = {
		TableName: database.Table[0].TableName, //table name : FLATS
		IndexName: database.Table[0].IndexName  //index name : priceSort
	};

	//Querry for getting flats   

    // get by hash key active
	if (data.active) {
		params["KeyConditionExpression"] = "#active = :active";
		params["ExpressionAttributeValues"] = { ":active": (data.active) };
		params["ExpressionAttributeNames"] = { "#active": "active" };

		//get by floor
		if (data.floor) {
			var str1 = data.floor;
			var arr1 = str1.split(",");
			console.log(arr1)
			var j1 = 1;
			for (var i1 = 0; i1 < arr1.length; i1++) {
			   if (params['FilterExpression'] == undefined) {
				  params['FilterExpression'] = "";
			   }
			   else {
				  params['FilterExpression'] += " or ";
			   }
			   params['FilterExpression'] += "#floor" + j1 + " IN (:floor" + j1 + ")";
			   params['ExpressionAttributeNames']["#floor" + j1] = "floor";
			   params['ExpressionAttributeValues'][":floor" + j1] = parseInt(arr1[i1]);
			   j1++;
			}
		 }
		// get flats by their flatTypes
		if (data.flattype) {
			if (params['FilterExpression'] == undefined) {
				params['FilterExpression'] = "";
			} else {
				params['FilterExpression'] += " and ";
			}
			params["FilterExpression"] += " #flattype =(:flattype)";
			params["ExpressionAttributeValues"][":flattype"] = data.flattype;
			params["ExpressionAttributeNames"]["#flattype"] = "flattype";
		}

		//get flats by their flat_status
		if (data.flat_status) {
			if (params['FilterExpression'] == undefined) {
				params['FilterExpression'] = "";
			}
			else {
				params['FilterExpression'] += " and ";
			}
			params["FilterExpression"] += " #flat_status =(:flat_status)";
			params["ExpressionAttributeValues"][":flat_status"] = data.flat_status;
			params["ExpressionAttributeNames"]["#flat_status"] = "flat_status";
		}

		//get by feature_name
		if(data.features){
			if(params['FilterExpression'] == undefined){
			  params['FilterExpression'] = ""
			}else{
			  params['FilterExpression'] += " and ";
			}
			feature_array = data.features.split(",");
			feature_array.forEach((element,i) => {
			  if(i>0){
				params['FilterExpression'] += " and ";
			  }
			  params['FilterExpression'] += "contains(#features"+i+",:features"+i+")"
			  params['ExpressionAttributeNames']["#features"+i] = "features";
			  params['ExpressionAttributeValues'][":features"+i] = element;
			});
		  }

		//sort flats according to their price ascending or descending 
		if ((data.priceSort) == "true") {
			params['ScanIndexForward'] = true;
		} else {
			params['ScanIndexForward'] = false;
		}
		return new Promise((resolve, reject) => {
			//limit
			params['Limit'] = data.limit;
			//pagination
			if (data.lastEvaluatedKeyid && data.lastEvaluatedKeyactive && data.lastEvaluatedKeyprice) {
				try {
					params['ExclusiveStartKey'] = {
						active: data.lastEvaluatedKeyactive,
						id: data.lastEvaluatedKeyid,
						price: parseFloat(data.lastEvaluatedKeyprice)
					};
				} catch (error) {
					reject("Invalid last evaluated keys");
					return;
				}
			}
			console.log("get_data", params);
			docClient.query(params, function (err, data) {
				if (err) {
					reject(err);//error
				} else {
					console.log(data);
					var container = {};
					container['page'] = data.LastEvaluatedKey;
					container['data'] = data.Items;
					resolve(container);//get successful
				}
			});
		})
	} else {
		//scan flats from their fields
		if (data.floor || data.flattype || data.features || data.flat_status || (data.lastEvaluatedKeyid && data.lastEvaluatedKeyactive && data.lastEvaluatedKeyprice)) {
			
			if (data.floor) {
				var str1 = data.floor;
				var arr1 = str1.split(",");
				console.log(arr1)
				var j1 = 1;
				for (var i1 = 0; i1 < arr1.length; i1++) {
				   if (params['FilterExpression'] == undefined) {
					  params['FilterExpression'] = "";
				   }
				   else {
					  params['FilterExpression'] += " or ";
				   }
				   params['FilterExpression'] += "#floor" + j1 + " IN (:floor" + j1 + ")";
				   if(params['ExpressionAttributeNames'] == undefined){
					   params['ExpressionAttributeNames'] = {};
				   }
				   if(params['ExpressionAttributeValues'] == undefined){
						params['ExpressionAttributeValues'] = {};
					}
				   params['ExpressionAttributeNames']["#floor" + j1] = "floor";
				   params['ExpressionAttributeValues'][":floor" + j1] = parseInt(arr1[i1]);
				   j1++;
				}
			 }
			//get by flatType
			if (data.flattype) {
				if ((params['FilterExpression'] || params['ExpressionAttributeValues'] || params['ExpressionAttributeNames']) == undefined) {
					params['FilterExpression'] = "";
					params['ExpressionAttributeValues'] = {};
					params['ExpressionAttributeNames'] = {};
				} else {
					params['FilterExpression'] += " and ";
				}
				params['FilterExpression'] += "#flattype IN (:flattype) "
				params['ExpressionAttributeValues'][":flattype"] = data.flattype;
				params['ExpressionAttributeNames']["#flattype"] = "flattype";
			}

			//get by FeatureName
				if(data.features){
					if(params['FilterExpression'] == undefined){
					  params['FilterExpression'] = ""
					}else{
					  params['FilterExpression'] += " and ";
					}
					feature_array = data.features.split(",");
					feature_array.forEach((element,i) => {
					  if(i>0){
						params['FilterExpression'] += " and ";
					  }
					  params['FilterExpression'] += "contains(#features"+i+",:features"+i+")"
					  if(params['ExpressionAttributeNames'] == undefined){
						params['ExpressionAttributeNames'] = {};
					}
					if(params['ExpressionAttributeValues'] == undefined){
						 params['ExpressionAttributeValues'] = {};
					 }
					  params['ExpressionAttributeNames']["#features"+i] = "features";
					  params['ExpressionAttributeValues'][":features"+i] = element;
					});
				  }
			//get by flatStatus
			if (data.flat_status) {
				if ((params['FilterExpression'] || params['ExpressionAttributeValues'] || params['ExpressionAttributeNames']) == undefined) {
					params['FilterExpression'] = "";
					params['ExpressionAttributeValues'] = {};
					params['ExpressionAttributeNames'] = {};
				} else {
					params['FilterExpression'] += " and ";
				}
				params['FilterExpression'] += "#flat_status IN (:flat_status) "
				params['ExpressionAttributeValues'][":flat_status"] = (data.flat_status)
				params['ExpressionAttributeNames']["#flat_status"] = "flat_status";
			}
		}
		return new Promise((resolve, reject) => {
			params['Limit'] = data.limit;
			if (data.lastEvaluatedKeyid && data.lastEvaluatedKeyactive && data.lastEvaluatedKeyprice) {
				try {
					params['ExclusiveStartKey'] = {
						active: data.lastEvaluatedKeyactive,
						id: data.lastEvaluatedKeyid,
						price: parseFloat(data.lastEvaluatedKeyprice)
					};
				} catch (error) {
					reject("Invalid last evaluated keys");
					return;
				}
			}
			console.log(params, "scan");
			docClient.scan(params, function (err, data) {
				if (err) {
					reject(err);
				} else {
					console.log(data);
					var container = {};
					container['page'] = data.LastEvaluatedKey;
					container['data'] = data.Items;
					resolve(container); //getting successfully
				}
			});
		})
	}
}
