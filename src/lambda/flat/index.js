//  try{
// let GET = require('./GET');
// let POST = require('./POST');
// let UPDATE = require('./UPDATE');
// let DELETE = require('./DELETE');
let DUMP = require('./DUMP');
//  }catch(e){console.log(e);}


exports.handler = function (event, context, callback) {
	switch (event.httpMethod) {
		case 'GET': require('./GET').execute(event.queryStringParameters, callback);
			break;
		case 'POST': require('./POST').execute(event.body, callback);
			break;
		 case 'PUT': require('./UPDATE').execute(event.body,event.headers,callback);
		 			break;
		 case 'DELETE': require('./DELETE').execute(event.body,callback);
				break;
		default: DUMP.execute({}, callback);
	}
}
