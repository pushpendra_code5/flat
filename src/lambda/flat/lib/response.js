
module.exports = function (execution,callback) {
	var response = {};
	console.log(execution);
	if(execution.err){
		response = {
			statusCode: execution.code || 406,
		    headers: execution.headers ||{
					'xCustomHeader': 'my custom header value',
					"Access-Control-Allow-Origin":"*",
					"Access-Control-Allow-Methods":"DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
					"Access-Control-Allow-Headers":"Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token"
		    },
		    body: JSON.stringify(execution.err||{"error":"Some Error Occured"})
		};
	}else{
		response = {
			statusCode: execution.code || 200,
		    headers: execution.headers ||{
		      'xCustomHeader': 'my custom header value',
					"Access-Control-Allow-Origin":"*",
					"Access-Control-Allow-Methods":"DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
					"Access-Control-Allow-Headers":"Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token"
		    },
		    body: JSON.stringify(execution.body) || {}
		};
	}
	console.log(response);
	callback(null,response);
}